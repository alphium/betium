extern crate termion;

// Termion
use termion::color;
use termion::clear;

mod write;
mod read;

// File Stream
use std::fs;

use std::io::Result;

fn separate_by_line<'a>(data: &String) -> Vec<String> {
	let mut strings: Vec<String> = Vec::new();
	strings = data.split("\n").map(|strings| strings.to_string()).collect::<Vec<_>>();
	return strings;
}

fn delete_task(line_number: usize, processed_data: &mut Vec<String>) -> &Vec<String> {
	processed_data.remove(line_number);
	return processed_data;
}

// TODO: Clean up main function

fn main() {
	
	let filename = String::from("./file.txt");
	let data: String = read::read_file(&filename);
	let mut processed_data = separate_by_line(&data);
	processed_data = delete_task(1, &mut processed_data).to_vec();
	processed_data = delete_task(2, &mut processed_data).to_vec();
	processed_data = delete_task(3, &mut processed_data).to_vec();
	write::write_file_final(&filename, processed_data);
}





