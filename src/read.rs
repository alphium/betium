use std::fs;

pub fn read_file(filename: &String) -> String {
    let data: String;
    data = fs::read_to_string(filename).expect("Could not read file...");
	return data;
}