use std::fs;

pub fn write_file(filename: &String, data: String, current_data: String) {
	let data_to_write: String = format!("{}\n{}", current_data, data);
    fs::write(filename, data_to_write).expect("Error ocurred while opening file...");
}

pub fn write_file_final(filename: &String, data: Vec<String>) {
	let mut final_data: String = String::from("");
	for el in data {
		final_data = format!("{}\n{}", final_data, el);
	}
	println!("{}", final_data);
}